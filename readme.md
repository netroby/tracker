Tracker 问题跟踪系统
==================

这是一个采用 GPL V3协议发布的开源问题跟踪系统。它致力于提供一个简单，实用的BUG跟踪系统。

许可协议
--------------------------


将采用GPL V3协议和商业许可证发行，参见LICENSE.txt及附带的GPLV3.txt。

Tracker 采用了Zend Framework框架，所以附带了一份BSD许可协议，参见ZendFramework_LICENSE.txt。

商业许可证参见 BIZ_LICENSE.txt ，购买商业许可证请联系netroby@netroby.com

用户可以自由选择一种许可证。

商业许可证需支付一定费用，可无限制应用，无限制修改代码，并且无需公开修改后的代码。

开源许可证，不收取费用，要求共享任何修改后的代码，同时我们接受任何形式的代码赞助，资金赞助（金额不限）。

项目招募
---------------

您可以通过下面的方式参与进来。

1. 为我们提供资金赞助 [netroby@netroby.com](mailto:netroby@netroby.com)
2. 帮助开发，请关注[https://github.com/netroby/tracker](https://github.com/netroby/tracker)
3. 提交需求建议，请发送邮件至：[netroby@netroby.com](mailto:netroby@netroby.com)
4. 提交BUG反馈，请在这里提交：[https://github.com/netroby/tracker/issues](https://github.com/netroby/tracker/issues)
5. 帮助撰写维基， 请关注：[https://github.com/netroby/tracker/wiki](https://github.com/netroby/tracker/wiki)
