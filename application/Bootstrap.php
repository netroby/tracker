<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	private $_config = null;
	private function _setConfig() {
		$this->_config = new Zend_Config($this->getOptions());
		Zend_Registry::set('config', $this->_config);
	}
	private function _getConfig() {
		if (null == $this->_config) {
			$this->_setConfig();
		}
		return $this->_config;
	}

	protected function _initDatabases()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		Zend_Registry::set('db', $db);
	}
	protected function _initLayout()
	{
		Zend_Layout::startMvc();
	}

	protected function _initLocale()
	{
		$config = $this->_getConfig();
		$options = $config->resources;
		$locale = new Zend_Locale($options->locale->default);
		Zend_Registry::set('Zend_Locale', $locale);
	}
	protected function _initTranslate()
	{
		$config = $this->_getConfig();
		$options = $config->resources;
		$session = new Zend_Session_Namespace('Translate');
		if ($session->locale) {
			$locale = $session->locale;
		} else {
			$locale = null;
			if (isset($options->locale->default))
			   	$locale = $options->locale->default;
		}
		$data = $options->translate->data->$locale;
		$translate = new Zend_Translate('array', $data, $locale);
		Zend_Registry::set('Zend_Translate', $translate);
	}
}

